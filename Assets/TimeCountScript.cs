﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR;

public class TimeCountScript : MonoBehaviour
{
    public float countTime;
    public GameObject tamaFactory;
    public GameObject restartTxt;
    public SteamVR_Input_Sources hand;
    public SteamVR_Action_Boolean grabAction;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (countTime > 0f){
            countTime -= Time.deltaTime; //スタートしてからの秒数を格納
            GetComponent<Text>().text = countTime.ToString("0"); //小数2桁にして表示
        }else
        {

            Debug.Log("0!!");
            Destroy(tamaFactory);
            GameObject[] tamas = GameObject.FindGameObjectsWithTag("Tama");
            foreach (GameObject tama in tamas) {
                Destroy(tama);
            }
            restartTxt.SetActive (true);
            if (grabAction.GetState(hand))
            {
                SceneManager.LoadScene ("MainScene");
            }
        }
    }
}
