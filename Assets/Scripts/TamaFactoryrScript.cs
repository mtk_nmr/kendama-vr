﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TamaFactoryrScript : MonoBehaviour
{
    public GameObject Tama;	

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnBall",0f,04);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnBall(){
		// Instantiate(Tama,new Vector3(Random.Range(-5f,5f),transform.position.y,transform.position.z),transform.rotation);
		Instantiate(Tama,new Vector3(Random.Range(-0.5f,0.5f),4f,transform.position.z+0.4f),transform.rotation);

	}
}
